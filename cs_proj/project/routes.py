from flask import render_template, redirect, flash, request, url_for
from project import app_in
from project.forms import LoginForm
from flask_login import login_required, current_user, login_user, logout_user
from project.models import User
from werkzeug.urls import url_parse
from project import db
from project.forms import RegistrationForm
from datetime import datetime
from project.forms import EditProfileForm

@app_in.route('/')
@app_in.route('/index')
#Protects pages from anonymous users 
@login_required
def index():
    posts = [
        {
            'author': {'username': 'John'},
            'body': 'Beautiful day in Portland!'
        },
        {
            'author': {'username': 'Susan'},
            'body': 'The Avengers movie was so cool!'
        }
    ]
    return render_template('index.html', title = 'Home', posts=posts)

@app_in.route('/login', methods = ['GET', 'POST'])
def login():
    #if user already logged in and goes to login page
    if current_user.is_authenticated:
        return redirect(url_for('index'))

    #Actually log in user
    form = LoginForm()
    if form.validate_on_submit():
        #Find user in database, either one or none in database (reason for using .first())
        #Check user credentials and redirect appropriately and registers user as logged in
        user = User.query.filter_by(username = form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid Username or Password')
            return redirect(url_for('login'))
        login_user(user, remember = form.remember_me.data)
        #Redirects to page user originally wanted to go to
        next_page = request.args.get('next')
        #If full url in nect argument (including domain name)
        if not next_page or url_parse(next_page).netloc != '':
            #Sets redirect to index
            next_page = url_for('index')
        return redirect(next_page)
    return render_template('login.html', title = 'Sign In', form = form)

@app_in.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))

@app_in.route('/register', methods = ['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username = form.username.data, email= form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, you are now a registered user!')
        return redirect(url_for('login'))
    return render_template('register.html', title = 'Register', form = form)

@app_in.route('/user/<username>')
@login_required
def user(username):
    user = User.query.filter_by(username=username).first_or_404()
    posts = [
        {'author': user, 'body' : 'Test post 1'},
        {'author': user, 'body' : 'Test post 2'}
    ]
    return render_template('user.html', user =user, posts=posts)

@app_in.before_request
def before_request():
    if current_user.is_authenticated:
        current_user.last_seen = datetime.utcnow()
        db.session.commit()

@app_in.route('/edit_profile', methods=['GET', 'POST'])
@login_required
def edit_profile():
    form = EditProfileForm()
    #If true copy data from form to user object and write to db
    if form.validate_on_submit():
        current_user.username = form.username.data
        current_user.about_me = form.about_me.data
        db.session.commit()
        flash("Your changes have been saved.")
        return redirect(url_for('edit_profile'))
    #Elif browser sends 'GET', send initial version of form/ prestored form
    #If 'POST' because error in form data
    elif request.method == 'GET':
        form.username.data = current_user.username
        form.about_me.data = current_user.about_me
    return render_template('edit_profile.html', title= 'Edit Profile', form = form)
