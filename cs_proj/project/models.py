from werkzeug.security import generate_password_hash, check_password_hash
from datetime import datetime
from project import db
from flask_login import UserMixin
from project import login
from hashlib import md5

#To set your own table name use the attribute __tablename__ 

class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key = True)
    username = db.Column(db.String(64), index = True, unique = True)
    email = db.Column(db.String(120), index = True, unique = True)
    password_hash = db.Column(db.String(128))
    posts = db.relationship('Post', backref ='author', lazy = 'dynamic')
    about_me = db.Column(db.String(140))
    last_seen = db.Column(db.DateTime, default = datetime.utcnow)
    
    #Create hash
    def set_password(self, password):
        self.password_hash = generate_password_hash(password)
    
    #Verify hash
    def check_password(self, password):
        return check_password_hash(self.password_hash, password)
    
    #Create avatar using gravatar
    def avatar(self, size):
        digest = md5(self.email.lower().encode('utf-8')).hexdigest()
        return 'https://www.gravatar.com/avatar/{}?d=identicon&s={}'.format(
            digest, size)

    def __repr__(self):
        return '<User {}>'.format(self.username)

#After modifying models use "flask db migrate" to generate a new migration script

#To apply the changes to the development database "flask db upgrade"

#To revert to a previous version use "flask db downgrade"

class Post(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    body = db.Column(db.String(140))
    timestamp = db.Column(db.DateTime, index = True, default=datetime.utcnow)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))

    def __repr__(self):
        return '<Post {}>'.format(self.body)
        return '<Post {}>'.format(self.body)

@login.user_loader
def load_user(id):
    return User.query.get(int(id))
