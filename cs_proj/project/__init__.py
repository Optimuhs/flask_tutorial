from flask import Flask
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager

#Import flask to set up variable
#Set variable name, Flask(__name__) will almost always properly set up
app_in = Flask(__name__)
app_in.config.from_object(Config)
db = SQLAlchemy(app_in)
migrate = Migrate(app_in, db)
#initialize extension
login = LoginManager(app_in)
login.login_view = 'login'
#Import from folder (.project), the routes file (routes.py)
from project import routes, models
