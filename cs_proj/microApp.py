#From folder import flask instance variable
from project import app_in, db
from project.models import User, Post

@app_in.shell_context_processor
def make_shell_context():
    return {'db':db, 'User':User,'Post':Post}
